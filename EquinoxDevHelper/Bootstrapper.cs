﻿using System;
using Autofac;
using Autofac.Builder;
using Catel.Services;
using EquinoxDevHelper.Credentials;
using EquinoxDevHelper.Services;
using EquinoxDevHelper.Services.Ip;
using EquinoxDevHelper.Services.VpnHelper;
using EquinoxDevHelper.ViewModels;

namespace EquinoxDevHelper
{
    public static class Bootstrapper
    {
        private static IContainer _container;

        public static IContainer Initialize()
        {
            var builder = new ContainerBuilder();
            builder.RegisterType<MainWindowViewModel>();
            builder.RegisterType<HelperService>();



            builder.AddSingleton<ConfigurationService>().As<IConfigurationService>();
            builder.AddSingleton<DispatcherService>().As<IDispatcherService>();
            builder.AddSingleton<VersionsViewModel>();
            builder.AddSingleton<KillerViewModel>();
            builder.AddSingleton<VpnViewModel>();
            builder.AddSingleton<VpnHelperService>();
            builder.AddSingleton<Acw21UpgradesViewModel>();
            builder.AddSingleton<UpgradesAcw21Service>();
            builder.AddSingleton<ProcessRunnerService>();
            builder.AddSingleton<CurrentIpInfoProvider>();
            builder.AddSingleton<OutlookService>();
            builder.AddSingleton<OutlookViewModel>();
            builder.AddSingleton<InstallerViewModel>();
            builder.AddSingleton<InstallerService>();
            builder.AddSingleton<BuildHelperViewModel>();
            builder.AddSingleton<DependencyRemovalService>();
            builder.AddSingleton<AcwCredentialManager>();

            _container = builder.Build();

            return _container;
        }

        public static void Destroy()
        {
            _container.Dispose<VpnViewModel>();
        }

        public static IRegistrationBuilder<TImplementer, ConcreteReflectionActivatorData, SingleRegistrationStyle> AddSingleton<TImplementer>(this ContainerBuilder builder)
        {
            return builder.RegisterType<TImplementer>().SingleInstance();
        }

        public static void Dispose<T>(this IContainer container) where T : IDisposable
        {
            container.Resolve<T>().Dispose();
        }

    }
}
