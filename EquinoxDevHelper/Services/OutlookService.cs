﻿using System;
using System.Linq;
using Catel.IO;
using Microsoft.Win32;

namespace EquinoxDevHelper.Services
{
    public class OutlookService
    {
        public string GetRootKeyName(string keyName)
        {
            var segments = keyName.Split('\\');
            if (segments.Length < 2)
            {
                throw new Exception();
            }
            return segments[0];
        }

        public string GetSubkeyName(string keyName)
        {
            var indexOfSlash = keyName.IndexOf('\\');
            if (indexOfSlash == -1)
            {
                throw new Exception();
            }
            return keyName.Substring(indexOfSlash+1);
        }

        public RegistryKey GetRootKeyFromName(string rootKeyName)
        {
            switch (rootKeyName)
            {
                case "HKEY_LOCAL_MACHINE":
                case "HKLM":
                    return RegistryKey.OpenBaseKey(RegistryHive.LocalMachine, RegistryView.Registry64);
                case "HKEY_CURRENT_USER":
                case "HKCU":
                    return RegistryKey.OpenBaseKey(RegistryHive.CurrentUser, RegistryView.Registry64);
                default:
                    throw new Exception("Failed to translate root key " + rootKeyName);
            }
        }

        private bool DeleteKey(string keyName)
        {
            var rootKeyName = GetRootKeyName(keyName);
            var rootKey = GetRootKeyFromName(rootKeyName);
            var subKey = GetSubkeyName(keyName);

            var key = rootKey.OpenSubKey(subKey);
            if (key == null)
            {
                return false;
            }
            rootKey.DeleteSubKeyTree(subKey);
            return true;
        }
        public int RemovePluginRegistry()
        {
            string[] keysToRemove =
            {
                @"HKCU\SOFTWARE\Avaya\Avaya Equinox.MSO.Addin",

                @"HKCU\SOFTWARE\Microsoft\Office\Outlook\Addins\Equinox.MSO.Addin",
                @"HKLM\SOFTWARE\WOW6432Node\Microsoft\Office\Outlook\Addins\Equinox.MSO.Addin",
                @"HKLM\SOFTWARE\Classes\Equinox.MSO.Addin",
                @"HKLM\SOFTWARE\Microsoft\Office\Outlook\Addins\Equinox.MSO.Addin",
                @"HKCU\SOFTWARE\Microsoft\Office\15.0\Outlook\Addins\Equinox.MSO.Addin",

                @"HKLM\SOFTWARE\Microsoft\Office\Outlook\Addins\Equinox.MSO.Addin.1"

            };

            return keysToRemove.Count(DeleteKey);
        }
    }
}
