﻿using System;
using System.Threading;
using System.Threading.Tasks;
using EquinoxDevHelper.Helpers;
using GitSharp;

namespace EquinoxDevHelper.Services
{
    public class RepositoryWatcher :IDisposable
    {
        private string _currentBranchName;
        private Repository _repository;
        private CancellationTokenSource _cancellationTokenSource;
        

        public event EventHandler<string> CurrentBranchNameChanged;
        public string CurrentBranchName;
         
        public void Start(string repositoryPath)
        {
            _cancellationTokenSource = new CancellationTokenSource();
            _repository = new Repository(repositoryPath);
            StartWatching(_cancellationTokenSource.Token);
        }

        public async void StartWatching(CancellationToken cancellationToken)
        {
            while (true)
            {
                var newName = _repository.CurrentBranch.Name;
                UpdateName(newName);
                await Task.Delay(TimeSpan.FromMilliseconds(500), cancellationToken);
            }
        }

        private void UpdateName(string newName)
        {
            if (!string.Equals(newName, _currentBranchName))
            {
                _currentBranchName = newName;
                CurrentBranchNameChanged?.Invoke(this, newName);
            }
        }


        public void Dispose()
        {
            _cancellationTokenSource.SafeCancel();
        }
    }
}
