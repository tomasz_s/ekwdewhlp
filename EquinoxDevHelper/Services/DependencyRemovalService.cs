﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using EquinoxDevHelper.Credentials;
using EquinoxDevHelper.DependencyProperties;
using EquinoxDevHelper.Helpers;

namespace EquinoxDevHelper.Services
{
    public class DependencyRemovalService
    {
        private readonly IConfigurationService _configurationService;
        private readonly HelperService _helperService;
        private readonly AcwCredentialManager _acwCredentialManager;

        private readonly Dictionary<AcwDependency, string> _acwDependenciesFolders = new Dictionary<AcwDependency, string>()
        {
            {AcwDependency.Sdk, "ClientSDK"},
            {AcwDependency.Uccl, "UCCL"},
            {AcwDependency.Ul, "UL"},
            {AcwDependency.OutlookPlugin, "OutlookPlugin"},
            {AcwDependency.WebExtension, "WebExtension"}
        };


        private readonly Dictionary<UcclDependency, string> _ucclDependenciesFolders = new Dictionary
            <UcclDependency, string>()
        {
            {UcclDependency.Sdk, "clientsdk"},
            {UcclDependency.Ul, "UL"},
            {UcclDependency.UriHandler, "UriHandler"}
        };
        public DependencyRemovalService(IConfigurationService configurationService, HelperService helperService, AcwCredentialManager acwCredentialManager)
        {
            _configurationService = configurationService;
            _helperService = helperService;
            _acwCredentialManager = acwCredentialManager;
        }

        public void RemoveAcwDependencies(IReadOnlyCollection<AcwDependency> dependenciesToRemove)
        {
            foreach (var acwDependency in dependenciesToRemove)
            {
                var fullPath = Path.Combine(_configurationService.GetAcwRepoPath(), "lib",
                    _acwDependenciesFolders[acwDependency]);
                if (Directory.Exists(fullPath))
                {
                    Directory.Delete(fullPath, true);
                }
            }
        }
        public void RemoveUcclDependencies(IReadOnlyCollection<UcclDependency> dependenciesToRemove)
        {
            foreach (var ucclDependency in dependenciesToRemove)
            {
                var fullPath = Path.Combine(_configurationService.GetUcclRepoPath(), "downloaded",
                    _ucclDependenciesFolders[ucclDependency]);
                if (Directory.Exists(fullPath))
                {
                    Directory.Delete(fullPath, true);
                }
            }
        }

        public void RemoveAllAcwDependencies()
        {
            RemoveAcwDependencies(EnumHelper.GetAllValues<AcwDependency>());
        }

        public void RemoveAllUcclDependencies()
        {
            RemoveUcclDependencies(new List<UcclDependency>
            {
                UcclDependency.Sdk,
                UcclDependency.Ul,
                UcclDependency.UriHandler
            });
        }

        private List<DirectoryInfo> EnumerateDirectoriesRecursively(DirectoryInfo directoryInfo)
        {
            return directoryInfo.EnumerateDirectories()
                .Concat(directoryInfo.EnumerateDirectories().SelectMany(EnumerateDirectoriesRecursively))
                .ToList();
        }

        public void RemoveAcwBinObj()
        {
            var acwFolder = _configurationService.GetAcwRepoPath();

            try
            {
                var di = new DirectoryInfo(acwFolder);

                var binAndObjFolders = EnumerateDirectoriesRecursively(di).Where(c => 
                c.FullName.EndsWith("\\bin") || 
                c.FullName.EndsWith("\\obj"));

                var fullNamesToRemove = binAndObjFolders.Select(f => f.FullName);

                var filesToRemove = string.Join(Environment.NewLine, fullNamesToRemove);

                var result =  MessageBox.Show("Following folders will be removed:" + Environment.NewLine + filesToRemove, "Bin and Obj to remove", MessageBoxButton.OKCancel);

                if (result == MessageBoxResult.OK)
                {
                    foreach (var binAndObjFolder in binAndObjFolders)
                    {
                        try
                        {
                            binAndObjFolder.Delete(true);
                        }
                        catch (Exception e)
                        {
                            throw;
                        }
                    }
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message, "Failed to remove all bin & obj folders");
            }

        }
        public event EventHandler<LogEntry> OnLog;

        private void Log(LogEntry logEntry)
        {
            OnLog?.Invoke(this, logEntry);
        }

        private void Log(string format, params object[] args)
        {
            OnLog?.Invoke(this, new LogEntry
            {
                DateTime = DateTime.Now,
                Entry = string.Format(format, args),
                IncludeEndline = true
            });
        }

        private void LogNoEndline(string format, params object[] args)
        {
            OnLog?.Invoke(this, new LogEntry
            {
                DateTime = DateTime.Now,
                Entry = string.Format(format, args),
                IncludeEndline = false
            });
        }

        private async Task<DirectoryRemoveResult> RemoveDirectory(string path)
        {
            try
            {
                if (!Directory.Exists(path))
                {
                    return DirectoryRemoveResult.NotExists;
                }

                Directory.Delete(path, true);
                return DirectoryRemoveResult.Ok;
            }
            catch (Exception ex)
            {
                return DirectoryRemoveResult.Failed;
            }
        }

        public async Task KillAcwRemoveConfiguration()
        {
            try
            {
                if (_helperService.IsEquinoxRunning())
                {
                    LogNoEndline("Killing Equinox... ");
                    if (await _helperService.KillEquinox())
                    {
                        Log("[OK]");
                        await Task.Delay(500);
                    }
                    else
                    {
                        Log("[Failed]");
                    }
                }

                var localConfigDirectory = "c:\\Users\\Tosc\\AppData\\Local\\Avaya\\Avaya Equinox\\";
                var roamingConfigDirectory = "c:\\Users\\Tosc\\AppData\\Roaming\\Avaya\\Avaya Equinox\\";

                LogNoEndline("Removing 'Local\\Avaya\\Avaya Equinox' ... ");
                Log("[{0}]", await RemoveDirectory(localConfigDirectory));

                LogNoEndline("Removing 'Roaming\\Avaya\\Avaya Equinox' ... ");
                Log("[{0}]", await RemoveDirectory(roamingConfigDirectory));
                _acwCredentialManager.OnLog += AcwCredentialManagerOnOnLog;
                _acwCredentialManager.RemoveAcwCredentials();
                _acwCredentialManager.OnLog -= AcwCredentialManagerOnOnLog;


                Log("Done");
            }
            catch (Exception ex)
            {
                Log("Error: {0}", ex.Message);
            }
        }

        private void AcwCredentialManagerOnOnLog(object sender, LogEntry logEntry)
        {
            Log(logEntry);
        }
    }
}
