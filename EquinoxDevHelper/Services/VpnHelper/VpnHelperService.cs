﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using Catel;

namespace EquinoxDevHelper.Services.VpnHelper
{

    // ReSharper disable once ClassNeverInstantiated.Global
    public class VpnHelperService
    {
        private readonly RouteHelper _routeHelper = new RouteHelper();

        public VpnHelperService()
        {
            if (CheckIfRoutesAreAlreadyAdded())
            {
                ChangeIsStarted(true);
            }
        }

        private bool CheckIfRoutesAreAlreadyAdded()
        {
            var routeList = _routeHelper.GetRoutes();
            if (routeList.Count(r => r.Metric == 99) > 100)
            {
                return true;
            }
            return false;
        }


        private void ChangeIsStarted(bool newIsStarted)
        {
            if (IsStarted == newIsStarted) return;

            IsStarted = newIsStarted;
            IsStartedChanged?.Invoke(this, EventArgs.Empty);
        }

        public event EventHandler IsStartedChanged; 
        public bool IsStarted { get; private set; }

        private static readonly int[] AvayaNetworkSegment = {135};
       

        private IEnumerable<int> GetSegmentsForRedirect()
        {
            return Enumerable.Range(0, 255).Except(AvayaNetworkSegment);
        }


        public Task StartVpnHelper()
        {
            var localInterface = NetHelper.GetLocalInterface();
            var virtualInterface = NetHelper.GetVirtualInterface();

            var gateway = NetHelper.GetDefaultGateway();

            var localInterfaceId = localInterface.GetIntefaceIndex();

            if (localInterfaceId == -1)
            {
                throw new Exception("Failed to find local interface index");
            }

            var segmentsForRedirect = GetSegmentsForRedirect();

            foreach (var networkSegment in segmentsForRedirect)
            {
                var addressToForward = $"{networkSegment}.0.0.0";
                var mask = "255.0.0.0";

                var route = new RouteInfo
                {
                    InterfaceIndex = localInterfaceId,
                    Destination = IPAddress.Parse(addressToForward),
                    Metric = 99,
                    NextHop = gateway,
                    Mask = IPAddress.Parse(mask)
                };

               _routeHelper.AddRoute(route);
            }

            var exceptions = new []
            {
                "10.135.144.253"
            };

            foreach (var exception in exceptions)
            {
                var route = new RouteInfo()
                {
                    InterfaceIndex = virtualInterface.GetIntefaceIndex(),
                    Destination = IPAddress.Parse(exception),
                    Metric = 99,
                    NextHop = null,
                    Mask = IPAddress.Parse("255.255.255.255")
                };

                _routeHelper.AddRoute(route);
            }
            ChangeIsStarted(true);
            return Task.FromResult(0);
        }

        public void StopVpnHelper()
        {
            var routeList = _routeHelper.GetRoutes().Where(r => r.Metric == 99);

            foreach (var route in routeList)
            {
                _routeHelper.DeleteRoute(route);
            }

            ChangeIsStarted(false);
        }

        private static void FixLocalInterfaceGateway(string interfaceIndex, string gw)
        {
            var mc = new ManagementClass("Win32_NetworkAdapterConfiguration");
            var managementObjectCollection = mc.GetInstances();
            foreach (var baseObject in managementObjectCollection)
            {
                var managementObject = (ManagementObject)baseObject;

                string[] gateways = managementObject["DefaultIPGateway"] as string[];
                if (managementObject["InterfaceIndex"].ToString() != interfaceIndex)
                {
                    continue;
                }

                if (gateways == null || !gateways.Contains(gw))
                {
                    managementObject.InvokeMethod("SetGateways", new object[]
                    {
                        new[] {gw}
                    });
                }
            }

        }

        private async Task FixLocalInterfaceGatewayTask(IPAddress localInterfaceId, IPAddress gatewayAddress, CancellationToken cancellationToken)
        {
            for (;;)
            {
                await Task.Delay(500, cancellationToken);
                FixLocalInterfaceGateway(localInterfaceId.ToString(), gatewayAddress.ToString());
            }
        }
    }

}
