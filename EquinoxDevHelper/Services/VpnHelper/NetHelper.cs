﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Management;
using System.Net;
using System.Net.NetworkInformation;
using System.Security.Principal;

namespace EquinoxDevHelper.Services.VpnHelper
{
    public static class NetHelper
    {
        public static IPAddress GetDefaultGateway()
        {
            ManagementObjectSearcher searcher = new ManagementObjectSearcher("select * from Win32_IP4RouteTable");

            foreach (var o in searcher.Get())
            {
                var adapterObject = (ManagementObject)o;
                string ipAddressString = adapterObject["NextHop"].ToString();

                IPAddress ipAddress;
                if (!IPAddress.TryParse(ipAddressString, out ipAddress))
                {
                    continue;
                }
                if (!ipAddress.Equals(IPAddress.Parse("0.0.0.0")))
                {
                    return ipAddress;
                }
            }
            return null;
        }

        public static IEnumerable<string> GetPhysicalInterfacesGuids()
        {
            var mos = new ManagementObjectSearcher(@"SELECT * 
                                     FROM   Win32_NetworkAdapter 
                                     WHERE  Manufacturer != 'Microsoft' 
                                            AND NOT PNPDeviceID LIKE 'ROOT\\%'");

            IList<ManagementObject> managementObjectList = mos.Get()
                                                  .Cast<ManagementObject>()
                                                  .OrderBy(p => Convert.ToUInt32(p.Properties["Index"].Value))
                                                  .ToList();

            return managementObjectList.Select(managementObject => managementObject["GUID"].ToString());
        }
        public static bool IsAdministrator()
        {
            WindowsIdentity identity = WindowsIdentity.GetCurrent();
            WindowsPrincipal principal = new WindowsPrincipal(identity);
            return principal.IsInRole(WindowsBuiltInRole.Administrator);
        }

        public static int GetIntefaceIndex(this NetworkInterface nic)
        {
            var ipProperties = nic.GetIPProperties();
            if (ipProperties == null)
            {
                return -1;
            }
            var ipv4Properties = ipProperties.GetIPv4Properties();
            if (ipv4Properties == null)
            {
                return -1;
            }
            return ipv4Properties.Index;
        }
        private static bool IsConnectorPresent(this NetworkInterface ni)
        {
            var scope = new ManagementScope(@"\\localhost\root\StandardCimv2");
            var query = new ObjectQuery($@"SELECT * FROM MSFT_NetAdapter WHERE ConnectorPresent = True AND DeviceID = '{ni.Id}'");
            using (var searcher = new ManagementObjectSearcher(scope, query))
            {
                var result = searcher.Get();
                return result.Count > 0;
            }
        }

        public static NetworkInterface GetLocalInterface()
        {
            var allInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            var localInterfaces = allInterfaces.Where(s => s.IsConnectorPresent()).ToList();
            return localInterfaces.FirstOrDefault();
        }

        public static NetworkInterface GetVirtualInterface()
        {
            var allInterfaces = NetworkInterface.GetAllNetworkInterfaces();
            var localInterfaces = allInterfaces.Where(s => !s.IsConnectorPresent()).ToList();
            return localInterfaces.FirstOrDefault();
        }

        public static IPAddress GetIpv4Address(this NetworkInterface networkInterface)
        {
            if (networkInterface == null)
            {
                return null;
            }
            if (networkInterface.NetworkInterfaceType == NetworkInterfaceType.Wireless80211 ||
                networkInterface.NetworkInterfaceType == NetworkInterfaceType.Ethernet)
            {
                foreach (UnicastIPAddressInformation ip in networkInterface.GetIPProperties().UnicastAddresses)
                {
                    if (ip.Address.AddressFamily == System.Net.Sockets.AddressFamily.InterNetwork)
                    {
                        return ip.Address;
                    }
                }
            }
            return null;
        }
    }
}
