﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Runtime.InteropServices;

namespace EquinoxDevHelper.Services.VpnHelper
{
    public class RouteHelper
    {
        public List<RouteInfo> GetRoutes()
        {
            var fwdTable = IntPtr.Zero;
            int size = 0;

            var result = RouteInterop.GetIpForwardTable(fwdTable, ref size, true);
            fwdTable = Marshal.AllocHGlobal(size);

            result = RouteInterop.GetIpForwardTable(fwdTable, ref size, true);
            var str = (RouteInterop.IPForwardTable)Marshal.PtrToStructure(fwdTable, typeof(RouteInterop.IPForwardTable));

            var table = new RouteInterop.MIB_IPFORWARDROW[str.Size];

            var routeInfoList = new List<RouteInfo>();
            var p = fwdTable + Marshal.SizeOf(str.Size);
            for (int i = 0; i < str.Size; ++i)
            {
                table[i] = (RouteInterop.MIB_IPFORWARDROW)Marshal.PtrToStructure(p, typeof(RouteInterop.MIB_IPFORWARDROW));
                p += Marshal.SizeOf(typeof(RouteInterop.MIB_IPFORWARDROW));
                var routeInfo = new RouteInfo
                {
                    Destination = new IPAddress(table[i].dwForwardDest),
                    NextHop = new IPAddress(table[i].dwForwardNextHop),
                    Mask = new IPAddress(table[i].dwForwardMask),
                    InterfaceIndex = table[i].dwForwardIfIndex,
                    Metric = table[i].dwForwardMetric1
                };
                routeInfoList.Add(routeInfo);
            }
            return routeInfoList;
        }

        public bool AddRoute(RouteInfo routeInfo)
        {
            var route = new RouteInterop.MIB_IPFORWARDROW
            {
                dwForwardDest = BitConverter.ToUInt32( routeInfo.Destination.GetAddressBytes(), 0),
                dwForwardMask = BitConverter.ToUInt32(routeInfo.Mask.GetAddressBytes(), 0),
                dwForwardNextHop = routeInfo.NextHop == null? 0: BitConverter.ToUInt32(routeInfo.NextHop.GetAddressBytes(), 0),
                dwForwardMetric1 = routeInfo.Metric,
                dwForwardType = RouteInterop.ForwardType.Indirect,
                dwForwardProto = RouteInterop.ForwardProtocol.NetMGMT,
                dwForwardAge = 0,
                dwForwardIfIndex = routeInfo.InterfaceIndex
            };

            var result = RouteInterop.CreateIpForwardEntry(ref route);

            if (result != 0)
            {
                return false;
            }
            return true;
        }

        public bool DeleteRoute(RouteInfo routeInfo)
        {
            var route = new RouteInterop.MIB_IPFORWARDROW
            {
                dwForwardDest = BitConverter.ToUInt32(routeInfo.Destination.GetAddressBytes(), 0),
                dwForwardMask = BitConverter.ToUInt32(routeInfo.Mask.GetAddressBytes(), 0),
                dwForwardNextHop = BitConverter.ToUInt32(routeInfo.NextHop.GetAddressBytes(), 0),
                dwForwardMetric1 = routeInfo.Metric,
                dwForwardType = RouteInterop.ForwardType.Indirect,
                dwForwardProto = RouteInterop.ForwardProtocol.NetMGMT,
                dwForwardAge = 0,
                dwForwardIfIndex = routeInfo.InterfaceIndex
            };

            var result = RouteInterop.DeleteIpForwardEntry(ref route);

            if (result != 0)
            {
                return false;
            }
            return true;
        }
    }
}
