﻿using System.Net;

namespace EquinoxDevHelper.Services.VpnHelper
{
    public class RouteInfo
    {
        public int InterfaceIndex { get; set; }
        public int Metric { get; set; }

        public IPAddress Destination { get; set; }
        public IPAddress Mask { get; set; }
        public IPAddress NextHop { get; set; }
    }
}
