﻿using System;
using System.Drawing;

namespace EquinoxDevHelper.Services
{
    public class InstalledProduct
    {
        private readonly string _name;
        private readonly string _version;
        private readonly DateTime _installedTime;
        private readonly string _productCode;

        

        public InstalledProduct(string name, string version, DateTime installedTime, string productCode)
        {
            _name = name;
            _version = version;
            _installedTime = installedTime;
            _productCode = productCode;
        }
        public string ProductCode
        {
            get { return _productCode; }
        }
        public string Name
        {
            get { return _name; }
        }

        public string Version
        {
            get { return _version; }
        }

        public DateTime InstalledTime
        {
            get { return _installedTime; }
        }
    }
}
