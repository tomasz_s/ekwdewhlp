﻿using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EquinoxDevHelper.Services
{
    public class ProcessRunnerService
    {
        public async Task<ProcessResult> RunProcess(string command, string arguments, CancellationToken cancellationToken)
        {
            return await Task<ProcessResult>.Factory.StartNew(() =>
            {
                var process = new Process
                {
                    StartInfo =
                    {
                        FileName = command,
                        Arguments = arguments,
                        UseShellExecute = false,
                        RedirectStandardOutput = true,
                        RedirectStandardError = true,
                        CreateNoWindow = true
                    }
                };
                process.Start();


                string output = process.StandardOutput.ReadToEnd();
                string error = process.StandardError.ReadToEnd();
                process.WaitForExit();


                int exitCode = process.ExitCode;
                return new ProcessResult(exitCode, error, output);
            }, cancellationToken);
        }
    }
}
