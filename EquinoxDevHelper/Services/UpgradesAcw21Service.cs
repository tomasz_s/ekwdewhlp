﻿
using System.ComponentModel;
using System.IO;
using GitSharp.Core.Transport;
using Microsoft.Win32;

namespace EquinoxDevHelper.Services
{
    public enum RootKeyType
    {
        CurrentUser,
        LocalMachine
    }
    public class UpgradesAcw21Service
    {

        private static RegistryKey GetRegistryKey(RootKeyType type)
        {
            RegistryKey registryKey;
            switch (type)
            {
                case RootKeyType.CurrentUser:
                    registryKey = Registry.CurrentUser;
                    break;
                case RootKeyType.LocalMachine:
                    registryKey = Registry.LocalMachine;
                    break;
                default:
                    throw new InvalidEnumArgumentException("type", (int)type, typeof(RootKeyType));
            }
            return registryKey;
        }
        private void SaveValue<T>(RootKeyType type, string path, string key, T value)
        {
            var registryKey = GetRegistryKey(type);
            
                // ReSharper disable once PossibleNullReferenceException
                registryKey
                    .CreateSubKey(path)
                    .SetValue(key, value);
        }

        public void SetUpgradeTempFolder(string tempFolderPath)
        {
            SaveValue(RootKeyType.CurrentUser,"Software\\Avaya", "UpgradeTempFolder", tempFolderPath);
        }


        public void Copy21SettingsToTempFolder(string tempFolderPath)
        {
            DirectoryHelper.CopyDirectory(@"C:\Users\Tosc\AppData\Local\Avaya\AvayaCommunicator.exe_Url_xcqvon41hjvbvyb1chyrugbqtfyord21\1.0.0.0", 
                Path.Combine(tempFolderPath, "Local"));

            DirectoryHelper.CopyDirectory(@"C:\Users\Tosc\AppData\Roaming\Avaya\Avaya Communicator", 
                Path.Combine(tempFolderPath,"Roaming"));
        }
    }

}
