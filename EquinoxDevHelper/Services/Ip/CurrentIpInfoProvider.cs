﻿using System;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace EquinoxDevHelper.Services.Ip
{
    public class CurrentIpInfoProvider
    {
        private readonly Random _random = new Random();
        public async Task<IpInfo> GetCurrentIp(CancellationToken token)
        {
            try
            {
                var httpClient = new HttpClient();
                var downloadResult = await httpClient.GetStringAsync("http://ipinfo.io/json?" + _random.NextDouble());

                dynamic obj = JsonConvert.DeserializeObject(downloadResult);
                return new IpInfo
                {
                    IpAddress = obj.ip,
                    City = obj.city,
                    Country = obj.country,
                    Isp = obj.org
                };
            }
            catch (Exception)
            {

                return null;
            }
            
        }
    }
}
