﻿
namespace EquinoxDevHelper.Services.Ip
{
    public class IpInfo
    {
        public string IpAddress { get; set; }
        public string Country { get; set; }
        public string City { get; set; }
        public string Isp { get; set; }
    }
}
