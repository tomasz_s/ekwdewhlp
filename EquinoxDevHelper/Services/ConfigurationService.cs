﻿
using System.Configuration;

namespace EquinoxDevHelper.Services
{
    public class ConfigurationService : IConfigurationService
    {
        public string GetUcclRepoPath()
        {
            string value = ConfigurationManager.AppSettings["uccl_repo"];
            return value;
        }

        public string GetAcwRepoPath()
        {
            string value = ConfigurationManager.AppSettings["acw_repo"];

            return value;
        }

        public string GetAcwDependencyPropertiesPath()
        {
            return "build\\dependency.properties";
        }

        public string GetUcclDependencyPropertiesPath()
        {
            return "bamboo\\build.properties";
        }
    }
}
