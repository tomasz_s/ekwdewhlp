﻿namespace EquinoxDevHelper.Services
{
    public class ProcessResult
    {
        public ProcessResult(int exitCode, string errorOutput, string output)
        {
            ExitCode = exitCode;
            ErrorOutput = errorOutput;
            Output = output;
        }

        public string ErrorOutput { get; }

        public string Output { get; }

        public int ExitCode { get; }
    }
}