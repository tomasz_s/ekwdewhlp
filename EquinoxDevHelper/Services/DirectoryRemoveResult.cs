﻿namespace EquinoxDevHelper.Services
{
    public enum DirectoryRemoveResult
    {
        Ok,
        Failed,
        NotExists
    }
}
