﻿namespace EquinoxDevHelper.Services
{
    public interface IConfigurationService
    {
        string GetUcclRepoPath();
        string GetAcwRepoPath();
        string GetAcwDependencyPropertiesPath();
        string GetUcclDependencyPropertiesPath();
    }
}