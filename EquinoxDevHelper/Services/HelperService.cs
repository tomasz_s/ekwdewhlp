﻿using System;
using System.Diagnostics;
using System.Threading;
using System.Threading.Tasks;

namespace EquinoxDevHelper.Services
{
    public class HelperService
    {
        private readonly ProcessRunnerService _processRunnerService;

        public HelperService(ProcessRunnerService processRunnerService)
        {
            _processRunnerService = processRunnerService;
        }

        public async Task FlushDns()
        {
            var processResult = await _processRunnerService.RunProcess("ipconfig", "/flushdns", CancellationToken.None);
            if (!processResult.Output.Contains("Successfully flushed the DNS Resolver Cache"))
            {
                throw new Exception("Failed to flush dns: " + processResult.Output);
            }
        }

        public async Task KillVisualStudio()
        {
            await Task.Factory.StartNew(() =>
            {
                foreach (var process in Process.GetProcessesByName("devenv"))
                {
                    process.Kill();
                }
            });
        }

        public bool IsEquinoxRunning()
        {
            foreach (var process in Process.GetProcesses())
            {
                if (process.ProcessName.Contains("Equinox") && process.Id != Process.GetCurrentProcess().Id)
                {
                    return true;
                }
            }

            return false;
        }



        public async Task<bool> KillEquinox()
        {
            try
            {
                await Task.Factory.StartNew(() =>
                {
                    foreach (var process in Process.GetProcesses())
                    {
                        if (process.ProcessName.Contains("Equinox") && process.Id != Process.GetCurrentProcess().Id)
                        {
                            process.Kill();
                        }
                    }
                });
                return true;
            }
            catch (Exception e)
            {
                return false;
            }
          
        }

        public async Task KillEkwinox()
        {
            await Task.Factory.StartNew(() =>
            {
                foreach (var process in Process.GetProcesses())
                {
                    if (process.ProcessName.Contains("Ekwinox"))
                    {
                        process.Kill();
                    }
                }
            });
        }

        public async Task KillOutlook()
        {
            await Task.Factory.StartNew(() =>
            {
                foreach (var process in Process.GetProcesses())
                {
                    if (process.ProcessName.Contains("OUTLOOK"))
                    {
                        process.Kill();
                    }
                }
            });
        }
    }
}
