﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Deployment.WindowsInstaller;

namespace EquinoxDevHelper.Services
{
    public class InstallerService
    {
        [DllImport("msi.dll", CharSet = CharSet.Unicode)]
        static extern Int32 MsiGetProductInfo(string product, string property,[Out] StringBuilder valueBuf, ref Int32 len);

        [DllImport("msi.dll", SetLastError = true)]
        static extern int MsiEnumProducts(int iProductIndex,StringBuilder lpProductBuf);

        private string GetProperty(string productCode, string property)
        {
            var productNameLen = 1024;
            var sbProductName = new StringBuilder(productNameLen);

            MsiGetProductInfo(productCode,
                property, sbProductName, ref productNameLen);
            return sbProductName.ToString();
        }

        public List<InstalledProduct> GetInstalledProducts()
        {
            var ret = new List<InstalledProduct>();
            var sbProductCode = new StringBuilder(100);

            int iIdx = 0;
            while (0 == MsiEnumProducts(iIdx++, sbProductCode))
            {
                var productCode = sbProductCode.ToString();

                var productName = GetProperty(productCode, "ProductName");
                var version = GetProperty(productCode, "VersionString");
                var installDateString = GetProperty(productCode, "InstallDate");
                DateTime installTime;
                DateTime.TryParseExact(installDateString,"yyyyMMdd", CultureInfo.InvariantCulture.DateTimeFormat, DateTimeStyles.None,  out installTime);

                ret.Add(new InstalledProduct(productName, version, installTime, productCode));
            }
            return ret.OrderBy(d=>d.Name).ToList();
        }

        public async Task UninstallProduct(InstalledProduct installedProduct)
        {
            await Task.Factory.StartNew(() =>
            {
                Installer.SetInternalUI(InstallUIOptions.Silent);
                Installer.ConfigureProduct(installedProduct.ProductCode, 0, InstallState.Absent, "");
            });
        }
    }
}
