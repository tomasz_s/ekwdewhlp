﻿using System.Windows;
using Autofac;
using EquinoxDevHelper.ViewModels;

namespace EquinoxDevHelper
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static IContainer Container { get; private set; }

        protected override void OnStartup(StartupEventArgs e)
        {
            Container = Bootstrapper.Initialize();

            var mainWindow = new MainWindow
            {
                DataContext = Container.Resolve<MainWindowViewModel>()
            };
            mainWindow.Show();
        }

        protected override void OnExit(ExitEventArgs e)
        {
            Bootstrapper.Destroy();
            base.OnExit(e);
        }
    }
}
