﻿using System.Windows;
using System.Windows.Controls;


namespace EquinoxDevHelper.CustomConstrol
{
    /// <summary>
    /// Interaction logic for DialogPopup.xaml
    /// </summary>
    public partial class DialogPopup:StackPanel
    {
        public DialogPopup()
        {
            InitializeComponent();
        }

        public static readonly DependencyProperty IsOpenProperty = DependencyProperty.Register(
            "IsOpen", typeof (bool), typeof (DialogPopup), new PropertyMetadata(default(bool)));

        public bool IsOpen
        {
            get { return (bool) GetValue(IsOpenProperty); }
            set { SetValue(IsOpenProperty, value); }
        }
    }
}
