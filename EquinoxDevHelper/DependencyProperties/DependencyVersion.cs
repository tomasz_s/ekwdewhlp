﻿
using System;

namespace EquinoxDevHelper.DependencyProperties
{
    public class DependencyVersion: IEquatable<DependencyVersion>
    {
        private string _version;
        private bool _isSnapshot;

        public static DependencyVersion Create(string version, bool isSnapshot)
        {
            return new DependencyVersion(version, isSnapshot);
        }

        public DependencyVersion(string version, bool isSnapshot)
        {
            _version = version;
            _isSnapshot = isSnapshot;
        }

        public string Version
        {
            get { return _version; }
            set { _version = value; }
        }

        public bool IsSnapshot
        {
            get { return _isSnapshot;  }
            set { _isSnapshot = value; }
        }

        public bool Equals(DependencyVersion other)
        {
            if (other == null)
            {
                return false;
            }
            return other.IsSnapshot == IsSnapshot && other.Version == Version;
        }

        public override string ToString()
        {
            if (IsSnapshot)
            {
                return $"{Version} SNAPSHOT";
            }
            return $"{Version} RELEASE";
        }
    }
}
