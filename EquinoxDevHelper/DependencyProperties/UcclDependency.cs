﻿
namespace EquinoxDevHelper.DependencyProperties
{
    public enum UcclDependency
    {
        Sdk,
        Ul,
        UriHandler,
        VersionCpp,
        AssemblyVersion,
        AssemblyFileVersion
    }
}
