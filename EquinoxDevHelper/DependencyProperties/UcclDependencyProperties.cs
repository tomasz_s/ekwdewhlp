﻿using System.Text.RegularExpressions;

namespace EquinoxDevHelper.DependencyProperties
{
    public class UcclDependencyProperties : LinkedDependencyWatcher<UcclDependency>
    {

        public UcclDependencyProperties(string dependencyPropertiesPath, string versionCpppPath, string assemblyInfoPath) :
            base
            (
                new DependencyProperties<UcclDependency>
                (
                    dependencyPropertiesPath,
                    DependencyDescription<UcclDependency>.Create(UcclDependency.Sdk, "clientSdkVersion","clientSdkVersionSuffix"),
                    DependencyDescription<UcclDependency>.Create(UcclDependency.Ul, "ulVersion", "ulVersionSufix"),
                    DependencyDescription<UcclDependency>.Create(UcclDependency.UriHandler, "UriHandlerVersion",null)
                ),
                new DependencyProperties<UcclDependency>
                (
                    versionCpppPath, 
                    DependencyDescription<UcclDependency>.FromRegex(UcclDependency.VersionCpp, @"#define\s*UCCL_VERSION\s*\""(.+)\""")
                ),
                new DependencyProperties<UcclDependency>
                (
                    assemblyInfoPath,
                    DependencyDescription<UcclDependency>.FromRegex(UcclDependency.AssemblyVersion, new Regex(@"^\s*\[assembly:\s*AssemblyVersion\(\""(.+)\""\)\]", RegexOptions.Multiline)),
                    DependencyDescription<UcclDependency>.FromRegex(UcclDependency.AssemblyFileVersion, new Regex(@"^\s*\[assembly:\s*AssemblyFileVersion\(\""(.+)\""\)\]", RegexOptions.Multiline))
                )
            )
        {
            
        }
    }
}
