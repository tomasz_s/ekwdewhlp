﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using Catel.Collections;

namespace EquinoxDevHelper.DependencyProperties
{
    public class LinkedDependencyWatcher<TDependencyType> : 
        IDependencyProperties<TDependencyType> where TDependencyType : struct, IConvertible
    {
        private readonly List<DependencyProperties<TDependencyType>> _linkedFiles;

        public LinkedDependencyWatcher(params DependencyProperties<TDependencyType>[] linkedFiles)
        {
            _linkedFiles = linkedFiles.ToList();
            _linkedFiles.ForEach( lf => lf.OnChanged += AnyFileChanged);
        }

        public string FirstFilePath
        {
            get { return _linkedFiles.First().DependencyPropertiesPath; }
        }

        private void AnyFileChanged(object sender, EventArgs eventArgs)
        {
            OnChanged?.Invoke(this, EventArgs.Empty);
        }

        public DependencyVersion GetDependencyVersion(TDependencyType acwDependency)
        {
            foreach (var dependencyPropertiese in _linkedFiles)
            {
                if (dependencyPropertiese.HandledDependencies.Contains(acwDependency))
                {
                    return dependencyPropertiese.GetDependencyVersion(acwDependency);
                }
            }
            return null;
        }


        public ReadOnlyDictionary<TDependencyType, DependencyVersion> GetAllDependencyVersions()
        {
            var allVersions = new Dictionary<TDependencyType, DependencyVersion>();
            foreach (var dependencyFile in _linkedFiles)
            {
                allVersions.AddRange(dependencyFile.GetAllDependencyVersions());
            }
            return new ReadOnlyDictionary<TDependencyType, DependencyVersion>(allVersions);
        }

        public List<TDependencyType> HandledDependencies
        {
            get
            {
                var allHandled = new List<TDependencyType>();
                foreach (var linkedFile in _linkedFiles)
                {
                    allHandled.AddRange(linkedFile.HandledDependencies);
                }
                return allHandled;
            }
        }

        public void SetDependencyVersion(TDependencyType dependency, DependencyVersion version)
        {
            foreach (var dependencyPropertiese in _linkedFiles)
            {
                if (dependencyPropertiese.HandledDependencies.Contains(dependency))
                {
                    dependencyPropertiese.SetDependencyVersion(dependency, version);
                }
            }
        }

        public void Load()
        {
            _linkedFiles.ForEach(f => f.Load());
        }

        public void Save()
        {
            _linkedFiles.ForEach(f => f.Save());
        }

        public void Dispose()
        {
            _linkedFiles.ForEach(f => f.Dispose());
        }

        public event EventHandler OnChanged;
    }
}
