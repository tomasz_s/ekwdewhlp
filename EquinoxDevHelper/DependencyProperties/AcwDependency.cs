﻿
namespace EquinoxDevHelper.DependencyProperties
{
    public enum AcwDependency
    {
        Uccl,
        Sdk,
        Ul,
        OutlookPlugin,
        WebExtension
    }
}
