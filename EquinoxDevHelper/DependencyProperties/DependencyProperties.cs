﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace EquinoxDevHelper.DependencyProperties
{
    public class DependencyProperties<TDependencyType>:IDisposable, 
        IDependencyProperties<TDependencyType> where TDependencyType: struct, IConvertible
    {
        private readonly string _filePath;
        private readonly List<DependencyDescription<TDependencyType>> _dependencyDescriptions;
        private readonly Dictionary<TDependencyType, DependencyVersion> _dependencyVersions = new Dictionary<TDependencyType, DependencyVersion>();
        private  FileSystemWatcher _fileSystemWatcher;
        private readonly object _fileLock = new object();

        public DependencyProperties(string filePath, params DependencyDescription<TDependencyType>[] dependencyDescriptions)
        {
            _filePath = filePath;
            _dependencyDescriptions = dependencyDescriptions.ToList();

            InitWatcher();
        }

        public List<TDependencyType> HandledDependencies
        {
            get { return _dependencyDescriptions.Select(d => d.DependencyType).ToList(); }
        }

        public string DependencyPropertiesPath => _filePath;

        private void InitWatcher()
        {
            string directoryToWatch = Path.GetDirectoryName(_filePath);
            if (directoryToWatch == null)
            {
                throw new InvalidOperationException();
            }
            var fileToWatch = Path.GetFileName(_filePath);

            _fileSystemWatcher = new FileSystemWatcher(directoryToWatch)
            {
                EnableRaisingEvents = true,
                NotifyFilter = NotifyFilters.LastWrite,
                Filter = fileToWatch
            };

            _fileSystemWatcher.Changed += FileSystemWatcherOnChanged;
        }

        private void DisposeWatcher()
        {
            _fileSystemWatcher.Changed -= FileSystemWatcherOnChanged;
            _fileSystemWatcher.Dispose();
        }

        private void FileSystemWatcherOnChanged(object sender, FileSystemEventArgs fileSystemEventArgs)
        {
            Load();
            OnChanged?.Invoke(this, EventArgs.Empty);
        }

        public DependencyVersion GetDependencyVersion(TDependencyType acwDependency)
        {
            return _dependencyVersions[acwDependency];
        }

        public ReadOnlyDictionary<TDependencyType, DependencyVersion> GetAllDependencyVersions()
        {
            return new ReadOnlyDictionary<TDependencyType, DependencyVersion>(_dependencyVersions);
        }

        public void SetDependencyVersion(TDependencyType dependency, DependencyVersion version)
        {
            _dependencyVersions[dependency] = version;
        }

        private string GetFileContent()
        {
            lock (_fileLock)
            {
                using (var fs = new FileStream(_filePath, FileMode.Open, FileAccess.Read))
                {
                    using (var sr = new StreamReader(fs))
                    {
                        return sr.ReadToEnd();
                    }
                }
            }
        }

        public void Load()
        {
            _dependencyVersions.Clear();

            string dependencyPropertiesContent = GetFileContent();

            foreach (var dependencyDescription in _dependencyDescriptions)
            {

                var mDependencyVersion = dependencyDescription.VersionRegex.Match(dependencyPropertiesContent);
                if (!mDependencyVersion.Success)
                {
                    continue;
                }
                var version = mDependencyVersion.Groups[1].Value;

                if (dependencyDescription.SuffixRegex != null)
                {
                    var mDependencySnapshot = dependencyDescription.SuffixRegex.Match(dependencyPropertiesContent);
                    if (mDependencySnapshot.Success && mDependencyVersion.Success)
                    {
                        var snapshot = mDependencySnapshot.Groups[1].Value;
                        _dependencyVersions.Add(dependencyDescription.DependencyType,
                            DependencyVersion.Create(version, snapshot == "-SNAPSHOT"));
                    }
                }
                else
                {
                    _dependencyVersions.Add(dependencyDescription.DependencyType,
                            DependencyVersion.Create(version, false));
                }
            }
        }

        public void Save()
        {
            var dependencyPropertiesContent = GetFileContent();

            foreach (var dependencyVersion in _dependencyVersions)
            {
                var dependencyDescription =
                    _dependencyDescriptions.FirstOrDefault(d => EqualityComparer<TDependencyType>.Default.Equals(d.DependencyType, dependencyVersion.Key));
                if (dependencyDescription == null)
                {
                    throw new InvalidOperationException();
                }

                var mDependencyVersion = dependencyDescription.VersionRegex.Match(dependencyPropertiesContent);
                if (mDependencyVersion.Success)
                {
                    ReplaceGrpup(mDependencyVersion.Groups[1], ref dependencyPropertiesContent,
                        dependencyVersion.Value.Version);
                }

                if (dependencyDescription.SuffixRegex != null)
                {

                    var mDependencySnapshot = dependencyDescription.SuffixRegex.Match(dependencyPropertiesContent);

                    if (mDependencySnapshot.Success)
                    {
                        ReplaceGrpup(mDependencySnapshot.Groups[1], ref dependencyPropertiesContent,
                            dependencyVersion.Value.IsSnapshot ? "-SNAPSHOT" : "");
                    }
                }
            }

            lock (_fileLock)
            {
                using (var fileStream = new FileStream(_filePath, FileMode.Create, FileAccess.Write))
                {
                    using (var textWriter = new StreamWriter(fileStream))
                    {
                        textWriter.Write("{0}", dependencyPropertiesContent);
                    }
                }
            }
        }


        private void ReplaceGrpup(Group group, ref string content, string newValue)
        {
            int originalLength = group.Length;
            int replacementLength = newValue.Length;

            var beforeText = content.Substring(0, group.Index);
            var afterText = content.Substring(group.Index + group.Length);
            content = $"{beforeText}{newValue}{afterText}";
        }

        #region [ DisposePattern ]

        public void Dispose()
        {
            Dispose(true);
        }

        public event EventHandler OnChanged;

        private void Dispose(bool isDisposing)
        {
            if (!isDisposing) return;
            DisposeWatcher();
        }

        #endregion
        
    }
}
