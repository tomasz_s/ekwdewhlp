﻿using System;
using System.Collections.Generic;
using System.Linq;
using Simple.CredentialManager;

namespace EquinoxDevHelper.Credentials
{
    public class AcwCredentialManager
    {
        public event EventHandler<LogEntry> OnLog;
        private void Log(string format, params object[] args)
        {
            OnLog?.Invoke(this, new LogEntry
            {
                DateTime = DateTime.Now,
                Entry = string.Format(format, args),
                IncludeEndline = true
            });
        }

        private void LogNoEndline(string format, params object[] args)
        {
            OnLog?.Invoke(this, new LogEntry
            {
                DateTime = DateTime.Now,
                Entry = string.Format(format, args),
                IncludeEndline = false
            });
        }

        private IEnumerable<Credential> GetEquinoxCredentials()
        {
            foreach (var credential in Credential.LoadAll())
            {
                if (credential?.Target?.Contains("Equinox") ?? false)
                {
                    yield return credential;
                }
            }
        }

        public void RemoveAcwCredentials()
        {

            var allCreds = GetEquinoxCredentials().ToList();

            if (!allCreds.Any())
            {
                Log("No Equinox credentials found");
                return;
            }
            foreach (var credential in allCreds)
            {
                LogNoEndline("Removing '{0}' credential... ", credential.Target);
                Log(credential.Delete() ? "[OK]" : "[Failed]");
            }
        }
    }
}
