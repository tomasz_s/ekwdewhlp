﻿using System;
using System.Threading.Tasks;
using System.Windows;
using Catel.MVVM;

namespace EquinoxDevHelper.Commands
{
    public class AsyncCommand<TParam> : Command<TParam>
    {
        private readonly Func<Task> _task;
        private bool _isExecuting;

        public AsyncCommand(Func<Task> task, Func<bool> canExecute = null) : base(null, canExecute)
        {
            _task = task;
        }

        public override bool CanExecute(TParam parameter)
        {
            return !_isExecuting && base.CanExecute(parameter);
        }

        protected override async void Execute(TParam parameter, bool ignoreCanExecuteCheck)
        {
            if (_isExecuting)
            {
                return;
            }
            _isExecuting = true;
            RaiseCanExecuteChanged();
            try
            {
                await _task();
            }
            catch (TaskCanceledException)
            {

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString(), "Error", MessageBoxButton.OK, MessageBoxImage.Error);
            }
            finally
            {
                _isExecuting = false;
            }
            RaiseCanExecuteChanged();

        }
    }

    public class AsyncCommand : AsyncCommand<object>
    {
        public AsyncCommand(Func<Task> task, Func<bool> canExecute) : base(task, canExecute)
        {

        }
        public AsyncCommand(Func<Task> task) : base(task, () => true)
        {

        }
    }
}
