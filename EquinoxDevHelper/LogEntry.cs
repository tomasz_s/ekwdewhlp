﻿using System;

namespace EquinoxDevHelper
{
    public class LogEntry
    {
        public string Entry { get; set; }
        public bool IncludeEndline { get; set; } = true;
        public DateTime DateTime;
    }
}
