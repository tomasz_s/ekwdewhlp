﻿using System.Threading;

namespace EquinoxDevHelper.Helpers
{
    public static class CancellationExtensions
    {
        public static void SafeCancel(this CancellationTokenSource cts)
        {
            if (cts == null)
            {
                return;
            }
            if (cts.IsCancellationRequested)
            {
                return;
            }
            cts.Cancel();
        }
    }
}
