﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace EquinoxDevHelper.Helpers
{
    public static class EnumHelper
    {
        public static IReadOnlyCollection<TEnum> GetAllValues<TEnum>()
        {
            return Enum.GetValues(typeof (TEnum)).Cast<TEnum>().ToList();
        }
    }
}
