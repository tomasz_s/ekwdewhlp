﻿using EquinoxDevHelper.DependencyProperties;

namespace EquinoxDevHelper.ViewModels
{
    public class DependencyViewModel
    {
        private readonly string _dependencyName;
        private readonly DependencyVersion _dependencyVersion;

        public DependencyViewModel(string dependencyName, DependencyVersion dependencyVersion)
        {
            _dependencyName = dependencyName;
            _dependencyVersion = dependencyVersion;
        }

        public string DependencyName
        {
            get { return _dependencyName; }
        }

        public string Version
        {
            get { return _dependencyVersion.Version; }
            set { _dependencyVersion.Version = value; }
        }

        public bool IsSnapshot
        {
            get { return _dependencyVersion.IsSnapshot; }
            set { _dependencyVersion.IsSnapshot = value; }
        }
    }
}
