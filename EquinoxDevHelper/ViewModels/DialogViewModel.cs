﻿using Catel.Data;
using Catel.MVVM;

namespace EquinoxDevHelper.ViewModels
{
    public class DialogViewModel :ViewModelBase
    {
        #region Title property

        public string Title
        {
            get { return GetValue<string>(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        public static readonly PropertyData TitleProperty = RegisterProperty("Title", typeof (string));

        #endregion
    }
}
