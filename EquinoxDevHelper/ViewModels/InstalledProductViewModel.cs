﻿using Catel.Data;
using Catel.MVVM;
using EquinoxDevHelper.Services;

namespace EquinoxDevHelper.ViewModels
{
    public class InstalledProductViewModel : ViewModelBase
    {
        private readonly InstalledProduct _product;

        public InstalledProductViewModel(InstalledProduct product)
        {
            _product = product;
            ProductName = product.Name;
            Version = product.Version;
            InstalledDate = product.InstalledTime.ToShortDateString();
            ProductCode = product.ProductCode;
        }

        public InstalledProduct Product => _product;

        #region ProductName property

        public string ProductName
        {
            get { return GetValue<string>(ProductNameProperty); }
            set { SetValue(ProductNameProperty, value); }
        }

        public static readonly PropertyData ProductNameProperty = RegisterProperty("ProductName", typeof (string));

        #endregion

        #region Version property

        public string Version
        {
            get { return GetValue<string>(VersionProperty); }
            set { SetValue(VersionProperty, value); }
        }

        public static readonly PropertyData VersionProperty = RegisterProperty("Version", typeof (string));

        #endregion

        #region InstalledDate property

        public string InstalledDate
        {
            get { return GetValue<string>(InstalledDateProperty); }
            set { SetValue(InstalledDateProperty, value); }
        }

        public static readonly PropertyData InstalledDateProperty = RegisterProperty("InstalledDate", typeof (string));

        #endregion

        #region ProductCode property

        public string ProductCode
        {
            get { return GetValue<string>(ProductCodeProperty); }
            set { SetValue(ProductCodeProperty, value); }
        }

        public static readonly PropertyData ProductCodeProperty = RegisterProperty("ProductCode", typeof (string));

        #endregion


    }
}
