﻿using System;
using Avaya.UCC.Nexus;
using Catel.Data;
using Catel.IO;
using Catel.MVVM;
using EquinoxDevHelper.Services;

namespace EquinoxDevHelper.ViewModels
{
    public class BuildHelperViewModel :ViewModelBase
    {
        private readonly DependencyRemovalService _dependencyRemovalService;
        private readonly IConfigurationService _configurationService;

        public BuildHelperViewModel(DependencyRemovalService dependencyRemovalService, IConfigurationService configurationService)
        {
            _dependencyRemovalService = dependencyRemovalService;
            _configurationService = configurationService;
        }

        #region RemoveAcwDependencies command

        private Command _removeAcwDependenciesCommand;

        public Command RemoveAcwDependenciesCommand
        {
            get { return _removeAcwDependenciesCommand ?? (_removeAcwDependenciesCommand = new Command(RemoveAcwDependencies)); }
        }

        private void RemoveAcwDependencies()
        {
            _dependencyRemovalService.RemoveAllAcwDependencies();
        }

        #endregion

        #region RemoveUcclDependencies command

        private Command _removeUcclDependenciesCommand;

        public Command RemoveUcclDependenciesCommand
        {
            get { return _removeUcclDependenciesCommand ?? (_removeUcclDependenciesCommand = new Command(RemoveUcclDependencies)); }
        }

        private void RemoveUcclDependencies()
        {
            _dependencyRemovalService.RemoveAllUcclDependencies();
        }

        #endregion


        #region RemoveBinObj command

        private Command _removeBinObjCommand;

        public Command RemoveBinObjCommand
        {
            get { return _removeBinObjCommand ?? (_removeBinObjCommand = new Command(RemoveBinObj)); }
        }

        private void RemoveBinObj()
        {
            _dependencyRemovalService.RemoveAcwBinObj();
        }

        #endregion


        #region DownloadDependencies command

        private Command _downloadDependenciesCommand;

        public Command DownloadDependenciesCommand
        {
            get { return _downloadDependenciesCommand ?? (_downloadDependenciesCommand = new Command(DownloadDependencies)); }
        }

        private async void DownloadDependencies()
        {
            LogContent = "";
            var downloader = new AcwDependencyDownloader();

            var dependencyPath = Path.Combine(_configurationService.GetAcwRepoPath(),
                _configurationService.GetAcwDependencyPropertiesPath());

            await downloader.DeownloadEquinoxDependencies(dependencyPath, Path.Combine(_configurationService.GetAcwRepoPath(), "lib"));

        }

        private void DownloaderOnOnLog(object sender, LogEntry s)
        {

            var fullEntry = $"{s.Entry}";

            if (s.IncludeEndline)
            {
                LogContent += fullEntry + Environment.NewLine;
            }
            else
            {
                LogContent += fullEntry;
            }
        }

        #endregion

        #region LogContext property

        public string LogContent
        {
            get => GetValue<string>(LogContextProperty);
            set => SetValue(LogContextProperty, value);
        }

        public static readonly PropertyData LogContextProperty = RegisterProperty("LogContent", typeof(string));

        #endregion

        #region KillAcwRemoveConfiguration command

        private Command _killAcwRemoveConfigurationCommand;

        public Command KillAcwRemoveConfigurationCommand => _killAcwRemoveConfigurationCommand ?? (_killAcwRemoveConfigurationCommand = new Command(KillAcwRemoveConfiguration));

        private async void KillAcwRemoveConfiguration()
        {
            LogContent = "";
            _dependencyRemovalService.OnLog += DownloaderOnOnLog;
            await _dependencyRemovalService.KillAcwRemoveConfiguration();
            _dependencyRemovalService.OnLog -= DownloaderOnOnLog;

        }

        #endregion
    }
}
