﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Catel.Data;
using Catel.MVVM;
using EquinoxDevHelper.Services;
using GitSharp.Core.Util;
using MaterialDesignThemes.Wpf;

namespace EquinoxDevHelper.ViewModels
{
    public class InstallerViewModel:ViewModelBase
    {
        private readonly InstallerService _installerService;

        public InstallerViewModel(InstallerService installerService)
        {
            _installerService = installerService;
            RefreshInstalledProduct();
            OnSelectedProductChanged();
        }

        private void RefreshInstalledProduct()
        {
            InstalledProducts = _installerService.GetInstalledProducts()
                .Select(p => new InstalledProductViewModel(p))
                .Where(p => p.ProductName.IndexOf("Avaya", StringComparison.InvariantCultureIgnoreCase) != -1)
                .ToList();

            IsInstalledProductsEmpty = InstalledProducts.Count == 0;
        }

        #region InstalledProducts property

        public List<InstalledProductViewModel> InstalledProducts
        {
            get { return GetValue<List<InstalledProductViewModel>>(InstalledProductsProperty); }
            set { SetValue(InstalledProductsProperty, value); }
        }

        public static readonly PropertyData InstalledProductsProperty = RegisterProperty("InstalledProducts", typeof (List<InstalledProductViewModel>));

        #endregion

        #region Refresh command

        private Command _refreshCommand;

        public Command RefreshCommand
        {
            get { return _refreshCommand ?? (_refreshCommand = new Command(Refresh)); }
        }

        private void Refresh()
        {
            RefreshInstalledProduct();
        }

        #endregion

        #region SelectedProduct property

        public InstalledProductViewModel SelectedProduct
        {
            get { return GetValue<InstalledProductViewModel>(SelectedProductProperty); }
            set { SetValue(SelectedProductProperty, value); }
        }

        public static readonly PropertyData SelectedProductProperty = RegisterProperty("SelectedProduct", typeof (InstalledProductViewModel), null,
            (sender, e) => ((InstallerViewModel) sender).OnSelectedProductChanged());

        private void OnSelectedProductChanged()
        {
            UninstallCommand.RaiseCanExecuteChanged();
            if (SelectedProduct == null)
            {
                UninstallButtonTitle = "Select product to uninstall";
            }
            else
            {
                UninstallButtonTitle = $"Uninstall {SelectedProduct.ProductName} {SelectedProduct.Version}";
            }
        }

        #endregion

        #region Uninstall command

        private AsyncCommand _uninstallCommand;

        public AsyncCommand UninstallCommand => _uninstallCommand ?? (_uninstallCommand = new AsyncCommand(Uninstall, CanUninstall));
   
        private async Task Uninstall()
        {
            if (SelectedProduct == null)
            {
                return;
            }
            var installerProduct = SelectedProduct.Product;

            await RunDialogProgress(async () =>
            {
                await _installerService.UninstallProduct(installerProduct);
                RefreshInstalledProduct();
            }, 
            $"Uninstalling {installerProduct.Name} {installerProduct.Version}...");
        }


        private async Task RunDialogProgress(Func<Task> action, string progressTitle)
        {
            var dialogViewModel = new DialogViewModel
            {
                Title = progressTitle
            };
            await DialogHost.Show(dialogViewModel, "3" ,async delegate (object sender, DialogOpenedEventArgs args)
            {
                var session = args.Session;

                await action();

                session.Close();
            });
        }
        

        private bool CanUninstall()
        {
            return SelectedProduct != null;
        }

        #endregion

        #region UninstallButtonTitle property

        public string UninstallButtonTitle
        {
            get { return GetValue<string>(UninstallButtonTitleProperty); }
            set { SetValue(UninstallButtonTitleProperty, value); }
        }

        public static readonly PropertyData UninstallButtonTitleProperty = RegisterProperty("UninstallButtonTitle", typeof (string));

        #endregion

        #region TestProgress command

        private Command _testProgressCommand;

        public Command TestProgressCommand
        {
            get { return _testProgressCommand ?? (_testProgressCommand = new Command(TestProgress)); }
        }

        private async void TestProgress()
        {
            await RunDialogProgress(async () =>
            {
                await Task.Delay(3000);
            }, "Operation in progress...");
        }

        #endregion

        #region IsInstalledProductsEmpty property

        public bool IsInstalledProductsEmpty
        {
            get { return GetValue<bool>(IsInstalledProductsEmptyProperty); }
            set { SetValue(IsInstalledProductsEmptyProperty, value); }
        }

        public static readonly PropertyData IsInstalledProductsEmptyProperty = RegisterProperty("IsInstalledProductsEmpty", typeof (bool));

        #endregion
    }
}
