﻿using Catel.MVVM;

namespace EquinoxDevHelper.ViewModels
{
    // ReSharper disable once ClassNeverInstantiated.Global
    public class MainWindowViewModel :ViewModelBase
    {
        public MainWindowViewModel(
            VersionsViewModel versionsViewModel, 
            KillerViewModel killerViewModel, 
            VpnViewModel vpnViewModel, 
            Acw21UpgradesViewModel acw21UpgradesViewModel, 
            OutlookViewModel outlookViewModel, 
            InstallerViewModel installerViewModel,
            BuildHelperViewModel buildHelperViewModel)
        {
            VersionsViewModel = versionsViewModel;
            KillerViewModel = killerViewModel;
            VpnViewModel = vpnViewModel;
            Acw21UpgradesViewModel = acw21UpgradesViewModel;
            OutlookViewModel = outlookViewModel;
            InstallerViewModel = installerViewModel;
            BuildHelperViewModel = buildHelperViewModel;
        }

        public VersionsViewModel VersionsViewModel { get; }

        public KillerViewModel KillerViewModel { get; }

        public VpnViewModel VpnViewModel { get; }

        public Acw21UpgradesViewModel Acw21UpgradesViewModel { get; }

        public OutlookViewModel OutlookViewModel { get; }

        public InstallerViewModel InstallerViewModel { get; }

        public BuildHelperViewModel BuildHelperViewModel { get; }
    }
}
