﻿using System;
using System.Net;
using System.Net.NetworkInformation;
using System.Threading;
using System.Threading.Tasks;
using Catel.Data;
using Catel.MVVM;
using EquinoxDevHelper.Helpers;
using EquinoxDevHelper.Services.Ip;
using EquinoxDevHelper.Services.VpnHelper;

namespace EquinoxDevHelper.ViewModels
{
    public class VpnViewModel :ViewModelBase, IDisposable
    {
        private readonly VpnHelperService _vpnHelperService;
        private readonly CurrentIpInfoProvider _currentIpInfoProvider;

        private readonly NetworkInterface _localInterface;
        private readonly IPAddress _defaultGateway;
        private readonly CancellationTokenSource _cancellationTokenSource = new CancellationTokenSource();
        private DateTime _lastStartOrStop = DateTime.Now;

        public VpnViewModel(VpnHelperService vpnHelperService, CurrentIpInfoProvider currentIpInfoProvider)
        {
            _vpnHelperService = vpnHelperService;
            _vpnHelperService.IsStartedChanged += VpnHelperServiceOnIsStartedChanged;
            _currentIpInfoProvider = currentIpInfoProvider;

            _localInterface = NetHelper.GetLocalInterface();
            var ipAddress = _localInterface.GetIpv4Address();

            LocalInterfaceIp = ipAddress?.ToString();
            LocalInterfaceName = _localInterface?.Name;

            LocalInterfaceGuid = _localInterface?.Id;

            _defaultGateway = NetHelper.GetDefaultGateway();

            DefaultGetway = _defaultGateway?.ToString();
            UpdateIpTask();
            IsStarted = _vpnHelperService.IsStarted;
        }

        private void VpnHelperServiceOnIsStartedChanged(object sender, EventArgs eventArgs)
        {
            IsStarted = _vpnHelperService.IsStarted;
            _lastStartOrStop = DateTime.Now;
        }

        private AsyncCommand _startVpnHelperCommand;
        public AsyncCommand StartVpnHelperCommand => _startVpnHelperCommand ?? (_startVpnHelperCommand = new AsyncCommand(StartVpnHelper));
        private AsyncCommand _stopVpnHelperCommand;
        public AsyncCommand StopVpnHelperCommand => _stopVpnHelperCommand ?? (_stopVpnHelperCommand = new AsyncCommand(StopVpnHelper));

        private async Task StartVpnHelper()
        {
            await _vpnHelperService.StartVpnHelper();
        }

        private Task StopVpnHelper()
        {
            _vpnHelperService.StopVpnHelper();
            return Task.FromResult(0);
        }

        private async void UpdateIpTask()
        {
            try
            {
                var cancellationToken = _cancellationTokenSource.Token;
                do
                {
                    var ipInfo = await _currentIpInfoProvider.GetCurrentIp(CancellationToken.None);
                    if (ipInfo == null)
                    {
                        PublicIp = "-";
                        CityName = "-";
                        CountryName = "-";
                        Isp = "-";
                        continue;
                    }

                    PublicIp = ipInfo.IpAddress;
                    CityName = ipInfo.City;
                    CountryName = ipInfo.Country;
                    Isp = ipInfo.Isp;

                    var secondsSinceLastStop = (DateTime.Now - _lastStartOrStop).TotalSeconds;
                    if (secondsSinceLastStop < 5)
                    {
                        await Task.Delay(1000, cancellationToken);
                    }
                    else if (secondsSinceLastStop < 20)
                    {
                        await Task.Delay(5000, cancellationToken);
                    }
                    else
                    {
                        await Task.Delay(20000, cancellationToken);
                    }
                }
                while (true);
            }
            catch (Exception)
            {
                // ignore
            }
        }

        #region IsStarted property

        public bool IsStarted
        {
            get { return GetValue<bool>(IsStartedProperty); }
            set { SetValue(IsStartedProperty, value); }
        }

        public static readonly PropertyData IsStartedProperty = RegisterProperty("IsStarted", typeof (bool));

        #endregion

        #region LocalInterfaceIp property

        public string LocalInterfaceIp
        {
            get { return GetValue<string>(LocalInterfaceIpProperty); }
            set { SetValue(LocalInterfaceIpProperty, value); }
        }

        public static readonly PropertyData LocalInterfaceIpProperty = RegisterProperty("LocalInterfaceIp", typeof(string));

        #endregion

        #region LocalInterfaceName property

        public string LocalInterfaceName
        {
            get { return GetValue<string>(LocalInterfaceNameProperty); }
            set { SetValue(LocalInterfaceNameProperty, value); }
        }

        public static readonly PropertyData LocalInterfaceNameProperty = RegisterProperty("LocalInterfaceName", typeof(string));

        #endregion

        #region DefaultGetway property

        /// <summary>
        /// Gets or sets the DefaultGetway value.
        /// </summary>
        public string DefaultGetway
        {
            get { return GetValue<string>(DefaultGetwayProperty); }
            set { SetValue(DefaultGetwayProperty, value); }
        }

        /// <summary>
        /// DefaultGetway property data.
        /// </summary>
        public static readonly PropertyData DefaultGetwayProperty = RegisterProperty("DefaultGetway", typeof(string));

        #endregion

        #region IsAdministrator property

        public bool IsAdministrator
        {
            get { return GetValue<bool>(IsAdministratorProperty); }
            set { SetValue(IsAdministratorProperty, value); }
        }

        public static readonly PropertyData IsAdministratorProperty = RegisterProperty("IsAdministrator", typeof(bool));

        #endregion

        #region LocalInterfaceGuid property

        public string LocalInterfaceGuid
        {
            get { return GetValue<string>(LocalInterfaceGuidProperty); }
            set { SetValue(LocalInterfaceGuidProperty, value); }
        }

        public static readonly PropertyData LocalInterfaceGuidProperty = RegisterProperty("LocalInterfaceGuid", typeof(string));

        #endregion

        #region PublicIp property

        public string PublicIp
        {
            get { return GetValue<string>(PublicIpProperty); }
            set { SetValue(PublicIpProperty, value); }
        }

        public static readonly PropertyData PublicIpProperty = RegisterProperty("PublicIp", typeof (string));

        #endregion

        #region CityName property

        public string CityName
        {
            get { return GetValue<string>(CityNameProperty); }
            set { SetValue(CityNameProperty, value); }
        }

        public static readonly PropertyData CityNameProperty = RegisterProperty("CityName", typeof (string));

        #endregion

        #region CountryName property

        public string CountryName
        {
            get { return GetValue<string>(CountryNameProperty); }
            set { SetValue(CountryNameProperty, value); }
        }

        public static readonly PropertyData CountryNameProperty = RegisterProperty("CountryName", typeof (string));

        #endregion

        #region Isp property

        public string Isp
        {
            get { return GetValue<string>(IspProperty); }
            set { SetValue(IspProperty, value); }
        }

        public static readonly PropertyData IspProperty = RegisterProperty("Isp", typeof (string));

        #endregion

        #region [ DisposePattern ]

        public void Dispose()
        {
            Dispose(true);
        }

        private void Dispose(bool isDisposing)
        {
            if (!isDisposing) return;
            _cancellationTokenSource.SafeCancel();

        }

        #endregion
    }
}
