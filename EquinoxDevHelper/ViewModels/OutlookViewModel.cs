﻿using System.Threading.Tasks;
using System.Windows;
using Catel.MVVM;
using EquinoxDevHelper.Services;

namespace EquinoxDevHelper.ViewModels
{
    public class OutlookViewModel : ViewModelBase
    {
        private readonly OutlookService _outlookService;

        public OutlookViewModel(OutlookService outlookService)
        {
            _outlookService = outlookService;
        }

        #region RemoveRegistry command

        private Command _removeRegistryCommand;

        public Command RemoveRegistryCommand
        {
            get { return _removeRegistryCommand ?? (_removeRegistryCommand = new Command(RemoveRegistry)); }
        }

        private void RemoveRegistry()
        {
            var removedKeyCount = _outlookService.RemovePluginRegistry();
            MessageBox.Show("Removed keys: " + removedKeyCount);

        }

        #endregion

    }
}
