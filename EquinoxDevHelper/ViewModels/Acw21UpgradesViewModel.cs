﻿using Catel.Data;
using Catel.MVVM;
using EquinoxDevHelper.Services;

namespace EquinoxDevHelper.ViewModels
{
    public class Acw21UpgradesViewModel :ViewModelBase
    {
        private readonly UpgradesAcw21Service _upgradesAcw21Service;

        public Acw21UpgradesViewModel(UpgradesAcw21Service upgradesAcw21Service)
        {
            _upgradesAcw21Service = upgradesAcw21Service;
            UpgradeTempFolder = @"C:\Users\Tosc\Desktop\20settings";
        }

        #region UpgradeTempFolder property

        public string UpgradeTempFolder
        {
            get { return GetValue<string>(UpgradeTempFolderProperty); }
            set { SetValue(UpgradeTempFolderProperty, value); }
        }

        public static readonly PropertyData UpgradeTempFolderProperty = RegisterProperty("UpgradeTempFolder", typeof (string));

        #endregion


        #region SetRegistryUpgradeTempFolder command

        private Command _setRegistryUpgradeTempFolderCommand;

        public Command SetRegistryUpgradeTempFolderCommand
        {
            get { return _setRegistryUpgradeTempFolderCommand ?? (_setRegistryUpgradeTempFolderCommand = new Command(SetRegistryUpgradeTempFolder)); }
        }

        private void SetRegistryUpgradeTempFolder()
        {
            _upgradesAcw21Service.SetUpgradeTempFolder(UpgradeTempFolder);
        }

        #endregion


        #region Copy21SettingsToTempFolder command

        private Command _copy21SettingsToTempFolderCommand;

        public Command Copy21SettingsToTempFolderCommand
        {
            get { return _copy21SettingsToTempFolderCommand ?? (_copy21SettingsToTempFolderCommand = new Command(Copy21SettingsToTempFolder)); }
        }

        private void Copy21SettingsToTempFolder()
        {
            _upgradesAcw21Service.Copy21SettingsToTempFolder(UpgradeTempFolder);
        }

        #endregion
    }
}
