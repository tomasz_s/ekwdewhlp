﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using Catel.Collections;
using Catel.Data;
using Catel.MVVM;
using Catel.Services;
using EquinoxDevHelper.DependencyProperties;
using EquinoxDevHelper.Services;

namespace EquinoxDevHelper.ViewModels
{
    public class VersionsViewModel : ViewModelBase
    {
        private readonly IDispatcherService _dispatcherService;
        private readonly AcwDependencyProperties _acwDependencyProperties;
        private readonly UcclDependencyProperties _ucclDependencyProperties;

        private readonly RepositoryWatcher _acwRepositoryWatcher;
        private readonly RepositoryWatcher _ucclRepositoryWatcher;
       
        private readonly List<AcwDependency> _acwDependenciesToList = new List<AcwDependency>()
        {
            AcwDependency.Sdk,
            AcwDependency.OutlookPlugin,
            AcwDependency.WebExtension,
            AcwDependency.Ul
        };

        private readonly List<UcclDependency> _ucclDependencyToList = new List<UcclDependency>()
        {
            UcclDependency.Sdk,
            UcclDependency.VersionCpp,
            UcclDependency.AssemblyVersion,
            UcclDependency.AssemblyFileVersion
        };

        public VersionsViewModel(IConfigurationService configurationService, IDispatcherService dispatcherService)
        {
            _dispatcherService = dispatcherService;
            var acwDependencyPropertiesPath = Path.Combine(configurationService.GetAcwRepoPath(),
                configurationService.GetAcwDependencyPropertiesPath());

            var ucclDependencyPropertiesPath = Path.Combine(configurationService.GetUcclRepoPath(),
               configurationService.GetUcclDependencyPropertiesPath());

            var versionCppPath = Path.Combine(configurationService.GetUcclRepoPath(), "src/api/Version.cpp");
            var ucclAssemblyInfoPath = Path.Combine(configurationService.GetUcclRepoPath(), "platform/csharp/AvayaClientLibrary/Properties/AssemblyInfo.cs");

            _acwRepositoryWatcher = new RepositoryWatcher();
            _ucclRepositoryWatcher = new RepositoryWatcher();

            _acwRepositoryWatcher.CurrentBranchNameChanged += AcwRepositoryWatcherOnCurrentBranchNameChanged;
            _ucclRepositoryWatcher.CurrentBranchNameChanged += UcclRepositoryWatcherOnCurrentBranchNameChanged;

            _acwRepositoryWatcher.Start(configurationService.GetAcwRepoPath());


            AcwDependencies = new ObservableCollection<DependencyViewModel>();
            UcclDependencies = new ObservableCollection<DependencyViewModel>();


            if (Directory.Exists(configurationService.GetUcclRepoPath()))
            {
                _ucclRepositoryWatcher.Start(configurationService.GetUcclRepoPath());
                _ucclDependencyProperties = new UcclDependencyProperties(ucclDependencyPropertiesPath, versionCppPath, ucclAssemblyInfoPath);
                _ucclDependencyProperties.OnChanged += UcclDependenciesChanged;
                _ucclDependencyProperties.Load();
                RealoadDependencyViewModels(UcclDependencies, _ucclDependencyProperties, _ucclDependencyToList);
            }



            _acwDependencyProperties = new AcwDependencyProperties(acwDependencyPropertiesPath);
            _acwDependencyProperties.OnChanged += AcwDependenciesChanged;
            _acwDependencyProperties.Load();

            RealoadDependencyViewModels(AcwDependencies, _acwDependencyProperties, _acwDependenciesToList);
        }

        private void UcclRepositoryWatcherOnCurrentBranchNameChanged(object sender, string branchName)
        {
            _dispatcherService.BeginInvokeIfRequired(() => UcclBranchName = branchName);
        }

        private void AcwRepositoryWatcherOnCurrentBranchNameChanged(object sender, string branchName)
        {
            _dispatcherService.BeginInvokeIfRequired(() => AcwBranchName = branchName);
        }

        private void AcwDependenciesChanged(object sender, EventArgs eventArgs)
        {
            RealoadDependencyViewModels(AcwDependencies, _acwDependencyProperties, _acwDependenciesToList);
        }

        private void UcclDependenciesChanged(object sender, EventArgs eventArgs)
        {
            RealoadDependencyViewModels(UcclDependencies, _ucclDependencyProperties, _ucclDependencyToList);
        }


        void RealoadDependencyViewModels<T>(ObservableCollection<DependencyViewModel> dependencyViewModels,
            IDependencyProperties<T> dependencyProperties, List<T> dependencyToList) where T : struct, IConvertible
        {
            _dispatcherService.BeginInvokeIfRequired(() =>
            {
                var newDependencyViewModels = new List<DependencyViewModel>();

                foreach (var dependency in dependencyProperties.GetAllDependencyVersions().Where(d => dependencyToList.Contains(d.Key)))
                {
                    newDependencyViewModels.Add(new DependencyViewModel(dependency.Key.ToString(), dependency.Value));
                }

                dependencyViewModels.Clear();
                dependencyViewModels.AddRange(newDependencyViewModels);
            });
        }



        #region AcwDependencies property

        public ObservableCollection<DependencyViewModel> AcwDependencies
        {
            get { return GetValue<ObservableCollection<DependencyViewModel>>(AcwDependenciesProperty); }
            set { SetValue(AcwDependenciesProperty, value); }
        }

        public static readonly PropertyData AcwDependenciesProperty = RegisterProperty("AcwDependencies", typeof(ObservableCollection<DependencyViewModel>));

        #endregion


        #region UcclDependencies property

        public ObservableCollection<DependencyViewModel> UcclDependencies
        {
            get { return GetValue<ObservableCollection<DependencyViewModel>>(UcclDependenciesProperty); }
            set { SetValue(UcclDependenciesProperty, value); }
        }

        public static readonly PropertyData UcclDependenciesProperty = RegisterProperty("UcclDependencies", typeof(ObservableCollection<DependencyViewModel>));

        #endregion

        #region AcwBranchName property

        public string AcwBranchName
        {
            get { return GetValue<string>(AcwBranchNameProperty); }
            set { SetValue(AcwBranchNameProperty, value); }
        }

        public static readonly PropertyData AcwBranchNameProperty = RegisterProperty("AcwBranchName", typeof (string));

        #endregion

        #region UcclBranchName property

        public string UcclBranchName
        {
            get { return GetValue<string>(UcclBranchNameProperty); }
            set { SetValue(UcclBranchNameProperty, value); }
        }

        public static readonly PropertyData UcclBranchNameProperty = RegisterProperty("UcclBranchName", typeof (string));

        #endregion

        #region SaveVersions command

        private Command _saveVersionsCommand;

        public Command SaveVersionsCommand
        {
            get { return _saveVersionsCommand ?? (_saveVersionsCommand = new Command(SaveVersions)); }
        }

        private void SaveVersions()
        {
            _acwDependencyProperties.Save();
            if (_ucclDependencyProperties != null)
            {
                _ucclDependencyProperties.Save();
            }
        }

        #endregion
    }
}
