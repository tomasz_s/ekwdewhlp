﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Catel.MVVM;
using EquinoxDevHelper.Commands;
using EquinoxDevHelper.Services;

namespace EquinoxDevHelper.ViewModels
{
    public class KillerViewModel :ViewModelBase
    {
        private readonly HelperService _helperService;

        public KillerViewModel(HelperService helperService)
        {
            _helperService = helperService;
        }

        #region FlushDns command

        private AsyncCommand _flushDnsCommand;

        public AsyncCommand FlushDnsCommand
        {
            get { return _flushDnsCommand ?? (_flushDnsCommand = new AsyncCommand(FlushDns)); }
        }

        private async Task FlushDns()
        {
           await _helperService.FlushDns();
        }

        #endregion

        #region KillVisualStudio command

        private AsyncCommand _killVisualStudioCommand;

        public AsyncCommand KillVisualStudioCommand
        {
            get { return _killVisualStudioCommand ?? (_killVisualStudioCommand = new AsyncCommand(KillVisualStudio)); }
        }

        private async Task KillVisualStudio()
        {
            await _helperService.KillVisualStudio();
        }

        #endregion

        #region KillEquinox command

        private AsyncCommand _killEquinoxCommand;

        public AsyncCommand KillEquinoxCommand
        {
            get { return _killEquinoxCommand ?? (_killEquinoxCommand = new AsyncCommand(KillEquinox)); }
        }

        private async Task KillEquinox()
        {
            await _helperService.KillEquinox();
        }

        #endregion

        #region KillEkwinox command

        private AsyncCommand _killEkwinoxCommand;

        public AsyncCommand KillEkwinoxCommand
        {
            get { return _killEkwinoxCommand ?? (_killEkwinoxCommand = new AsyncCommand(KillEkwinox)); }
        }

        private async Task KillEkwinox()
        {
            await _helperService.KillEkwinox();
        }

        #endregion

        #region KillOutlook command

        private AsyncCommand _killOutlookCommand;

        public AsyncCommand KillOutlookCommand
        {
            get { return _killOutlookCommand ?? (_killOutlookCommand = new AsyncCommand(KillOutlook)); }
        }

        private async Task KillOutlook()
        {
            await _helperService.KillOutlook();
        }

        #endregion
    }
}
