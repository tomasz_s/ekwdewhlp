﻿
namespace Avaya.UCC.Nexus.DependencyProperties
{
    public class AcwDependencyProperties :LinkedDependencyWatcher<AcwDependency>
    {
        public AcwDependencyProperties(string dependencyPropertiesPath) :

            base
            (
                new DependencyProperties<AcwDependency>
                (
                    dependencyPropertiesPath,
                    DependencyDescription<AcwDependency>.Create(AcwDependency.Sdk, "clientSdkVersion","clientSdkVersionSuffix"),
                    DependencyDescription<AcwDependency>.Create(AcwDependency.Uccl, "ucclVersion", "ucclSnapshot"),
                    DependencyDescription<AcwDependency>.Create(AcwDependency.Ul, "ulVersion", "ulVersionSufix"),
                    DependencyDescription<AcwDependency>.Create(AcwDependency.OutlookPlugin, "outlookPluginVersion","outlookPluginSnapshot"),
                    DependencyDescription<AcwDependency>.Create(AcwDependency.WebExtension, "webExtensionVersion","webExtensionSnapshot"),
                    DependencyDescription<AcwDependency>.Create(AcwDependency.ScreenLink, "screenLinkVersion", "screenLinkSnapshot")

                 )
            )
        {

        }
    }
}
