﻿
using System;

namespace Avaya.UCC.Nexus.DependencyProperties
{
    public class DependencyVersion: IEquatable<DependencyVersion>
    {
        public static DependencyVersion Create(string version, bool isSnapshot)
        {
            return new DependencyVersion(version, isSnapshot);
        }

        public DependencyVersion(string version, bool isSnapshot)
        {
            Version = version;
            IsSnapshot = isSnapshot;
        }

        public string Version { get; set; }

        public bool IsSnapshot { get; set; }

        public bool Equals(DependencyVersion other)
        {
            if (other == null)
            {
                return false;
            }
            return other.IsSnapshot == IsSnapshot && other.Version == Version;
        }

        public override string ToString()
        {
            if (IsSnapshot)
            {
                return $"{Version} SNAPSHOT";
            }
            return $"{Version} RELEASE";
        }
    }
}
