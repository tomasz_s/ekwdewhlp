using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Avaya.UCC.Nexus.DependencyProperties
{
    public interface IDependencyProperties<TDependencyType> where TDependencyType : struct, IConvertible
    {
        DependencyVersion GetDependencyVersion(TDependencyType acwDependency);
        ReadOnlyDictionary<TDependencyType, DependencyVersion> GetAllDependencyVersions();
        List<TDependencyType> HandledDependencies { get; }
        void SetDependencyVersion(TDependencyType dependency, DependencyVersion version);
        void Load();
        void Save();
        void Dispose();

        event EventHandler OnChanged;
    }
}