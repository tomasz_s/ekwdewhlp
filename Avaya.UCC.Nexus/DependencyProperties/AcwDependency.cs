﻿
namespace Avaya.UCC.Nexus.DependencyProperties
{
    public enum AcwDependency
    {
        Uccl,
        Sdk,
        Ul,
        OutlookPlugin,
        WebExtension,
        ScreenLink
    }
}
