﻿using System;
using System.Text.RegularExpressions;

namespace Avaya.UCC.Nexus.DependencyProperties
{
    public class DependencyDescription<T>
    {
        private DependencyDescription(T dependencyType, Regex versionRegex, Regex suffixRegex)
        {
            DependencyType = dependencyType;
            VersionRegex = versionRegex;
            SuffixRegex = suffixRegex;
        }

        public static DependencyDescription<T> FromRegex(T type, string regex)
        {
            if (string.IsNullOrWhiteSpace(regex))
            {
                throw new ArgumentException(nameof(regex));
            }
            return new DependencyDescription<T>(type, new Regex(regex, RegexOptions.Compiled), null);
        }
        public static DependencyDescription<T> FromRegex(T type, Regex regex)
        {
            if (regex == null)
            {
                throw new ArgumentException(nameof(regex));
            }
            return new DependencyDescription<T>(type, regex, null);
        }
        public static DependencyDescription<T> Create(T dependencyType, string versionTag, string suffixTag)
        {
            if (string.IsNullOrWhiteSpace(versionTag))
            {
                throw new ArgumentNullException(nameof(dependencyType));
            }
            var reDependencyVersion = new Regex(versionTag + "=([^\r]*)", RegexOptions.Compiled);
            var reDependencySnapshot = string.IsNullOrEmpty(suffixTag) ? null : new Regex(suffixTag + "=([^\r]*)", RegexOptions.Compiled);

            return new DependencyDescription<T>(dependencyType, reDependencyVersion, reDependencySnapshot);
        }

        public T DependencyType { get; }

        public Regex VersionRegex { get; }

        public Regex SuffixRegex { get; }
    }
}
