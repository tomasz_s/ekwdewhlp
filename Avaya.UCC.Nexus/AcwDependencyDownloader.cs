﻿using System;
using System.Diagnostics;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Avaya.UCC.Nexus.DependencyProperties;

namespace Avaya.UCC.Nexus
{
    public class AcwDependencyDownloader
    {
        public event EventHandler<string> OnLog;

        private void Log(string format, params object[] args)
        {
            var logLine = $"{DateTime.Now:hh:mm:ss.fff} " + string.Format(format, args);
            OnLog?.Invoke(this, logLine);
        }

        NetworkCredential _networkCredential = new NetworkCredential("-", "-");


        private async Task<bool> DownloadFile(Uri uri, string targetFile)
        {
            try
            {
                Log("Downloading {0}", uri);
                Stopwatch sw = Stopwatch.StartNew();
                
                var clientHandler = new HttpClientHandler
                {
                    Credentials = _networkCredential
                };
                var httpClient = new HttpClient(clientHandler);
                
                using (var response = await httpClient.GetAsync(uri))
                {
                    response.EnsureSuccessStatusCode();

                    using (FileStream fileStream =
                        new FileStream(targetFile, FileMode.Create, FileAccess.Write, FileShare.None))
                    {
                        //copy the content from response to filestream
                        await response.Content.CopyToAsync(fileStream);
                    }
                }
                sw.Stop();
                Log("Done, took {0}", sw.Elapsed);

                return true;

            }
            catch (Exception e)
            {
                Log("Failed to download: " + e.ToString());
                return false;
            }
        }

        public async Task DeownloadEquinoxDependencies(string dependencyPropertyPath, string targetDirectory)
        {
            Log("Dependency property path: {0}", dependencyPropertyPath);

            var acwDependencyProperties = new AcwDependencyProperties(dependencyPropertyPath);
            acwDependencyProperties.Load();

            Log("Dependencies: ");
            foreach (var allDependencyVersion in acwDependencyProperties.GetAllDependencyVersions())
            {
                Log(" {0} {1} {2}", 
                    allDependencyVersion.Key.ToString().PadRight(17, ' '), 
                    allDependencyVersion.Value.Version.PadRight(10), 
                    allDependencyVersion.Value.IsSnapshot ? "SNAPSHOT": string.Empty);
            }

            Uri uri = new Uri(
                "https://nexus-ott.forge.avaya.com/repository/clientsdk-maven/com/avaya/clientservices/windows/AvayaClientServices/263.0.0/AvayaClientServices-263.0.0.zip");


            var targetDir = Path.Combine(targetDirectory, "AvayaClientServices.zip");

            await DownloadFile(uri, targetDir);
       ;
        }
    }
}
