﻿using System;
using System.IO;
using System.Net.Mime;
using EquinoxDevHelper.DependencyProperties;
using EquinoxDevHelper.Services;
using Moq;
using NUnit.Framework;

namespace EquinoxDevHelper.Tests
{
    [TestFixture]
    public class DependencyServicesTest
    {
        private AcwDependencyProperties CreateNew(string path = "dependency.properties")
        {
            var testPath = TestContext.CurrentContext.TestDirectory;
            var acwTestPath = Path.Combine(testPath, "Data");
            var depPropertyPath = path;
          

            return new AcwDependencyProperties(Path.Combine(acwTestPath, depPropertyPath));
        }

        [Test]
        public void A_LoadedVersionsAreSameAsInFile1()
        {
            var dependencyProperties = CreateNew();

            dependencyProperties.Load();

            Assert.AreEqual(
                DependencyVersion.Create("247.0.3", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.Sdk)
                );

            Assert.AreEqual(
                 DependencyVersion.Create("247.5.3", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.Uccl)
               );

            Assert.AreEqual(
                DependencyVersion.Create("3.0.6", false),
                dependencyProperties.GetDependencyVersion(AcwDependency.Ul)
                );

            Assert.AreEqual(
                DependencyVersion.Create("3.0.16", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.OutlookPlugin)
                );

            Assert.AreEqual(
                DependencyVersion.Create("3.0.0.0", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.WebExtension)
                );
        }

        [Test]
        public void A_LoadedVersionsAreSameAsInFile2()
        {
            var dependencyProperties = CreateNew("acw322.properties");

            dependencyProperties.Load();

            Assert.AreEqual(
                DependencyVersion.Create("252.2.10", false),
                dependencyProperties.GetDependencyVersion(AcwDependency.Sdk)
                );

            Assert.AreEqual(
                 DependencyVersion.Create("252.2.10", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.Uccl)
               );

            Assert.AreEqual(
                DependencyVersion.Create("3.0.6", false),
                dependencyProperties.GetDependencyVersion(AcwDependency.Ul)
                );

            Assert.AreEqual(
                DependencyVersion.Create("3.2.1", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.OutlookPlugin)
                );

            Assert.AreEqual(
                DependencyVersion.Create("3.0.0.0", true),
                dependencyProperties.GetDependencyVersion(AcwDependency.WebExtension)
                );
        }

        [Test]
        [TestCase("acw322.properties")]
        [TestCase("dependency.properties")]

        public void FileIsSameAfterLoadAndSave(string filePath)
        {
            var dependencyProperties = CreateNew(filePath);

            dependencyProperties.Load();
            var fileBefore = File.ReadAllText(dependencyProperties.FirstFilePath);

            dependencyProperties.Save();

            var fileAfter = File.ReadAllText(dependencyProperties.FirstFilePath);

            Assert.AreEqual(fileBefore, fileAfter);
        }

        [Test]
        public void CanChangeVersions()
        {
            var dependencyProperties = CreateNew();

            dependencyProperties.Load();
            dependencyProperties.SetDependencyVersion(AcwDependency.Uccl, DependencyVersion.Create("22.34.5", false));
            dependencyProperties.SetDependencyVersion(AcwDependency.Ul, DependencyVersion.Create("3.0.2", true));
            dependencyProperties.SetDependencyVersion(AcwDependency.WebExtension, DependencyVersion.Create("5.0.1.2", true));
            dependencyProperties.SetDependencyVersion(AcwDependency.Sdk, DependencyVersion.Create("3.55.23.2", false));
            dependencyProperties.SetDependencyVersion(AcwDependency.OutlookPlugin, DependencyVersion.Create("1.0.0.5", true));

            dependencyProperties.SetDependencyVersion(AcwDependency.OutlookPlugin, DependencyVersion.Create("1.0.0.1", false));
            dependencyProperties.Save();

            dependencyProperties.Load();

            Assert.AreEqual(DependencyVersion.Create("22.34.5", false), dependencyProperties.GetDependencyVersion(AcwDependency.Uccl));
            Assert.AreEqual(DependencyVersion.Create("3.0.2", true), dependencyProperties.GetDependencyVersion(AcwDependency.Ul));
            Assert.AreEqual(DependencyVersion.Create("5.0.1.2", true), dependencyProperties.GetDependencyVersion(AcwDependency.WebExtension));
            Assert.AreEqual(DependencyVersion.Create("3.55.23.2", false), dependencyProperties.GetDependencyVersion(AcwDependency.Sdk));
            Assert.AreEqual(DependencyVersion.Create("1.0.0.1", false), dependencyProperties.GetDependencyVersion(AcwDependency.OutlookPlugin));
        }

    }
}
